<?php


namespace BhanviJain\OnlineAgreementQueryAPI\Resolvers;


use GraphQL\Type\Definition\ResolveInfo;
use Kepawni\Serge\Infrastructure\GraphQL\TypeResolver;
use PDO;

class ProposalByIdResolver extends TypeResolver
{
    public function __construct(TypeResolver $base = null)
    {
        parent::__construct($base);
        // BASE RESOLVER
        $this->addResolverForField('CqrsQuery', 'getProposalById','getAggregateId');

        // RESOLVER FOR PROPOSAL->EMAIL
        $this->addResolverForField('Proposal', 'email','getEmailId');

        // RESOLVER FOR PROPOSAL->PROPOSALHASH
        $this->addResolverForField('Proposal', 'proposalHash','getProposal');

        // RESOLVER FOR PROPOSAL->FILE
        $this->addResolverForField('Proposal', 'file','getFile');

        // RESOLVER FOR PROPOSAL->INFO
        $this->addResolverForField('Proposal', 'info','getInfo');

        // RESOLVER FOR INFO->ATTRIBUTE
        $this->addResolverForField('KeyValuePair', 'attribute','getAttribute');

        // RESOLVER FOR INFO->VALUE
        $this->addResolverForField('KeyValuePair', 'value','getValue');

    }

}