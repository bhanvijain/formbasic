<?php


namespace BhanviJain\OnlineAgreementQueryAPI\Resolvers;


use GraphQL\Type\Definition\ResolveInfo;
use Kepawni\Serge\Infrastructure\GraphQL\TypeResolver;
use PDO;

class IsProposalConfirmedResolver extends TypeResolver
{
    public function __construct(TypeResolver $base = null)
    {
        parent::__construct($base);
        $this->addResolverForField('CqrsQuery', 'isProposalConfirmed','getIsConfirmed');
    }
}