<?php


namespace BhanviJain\OnlineAgreementQueryAPI\Resolvers;


use GraphQL\Type\Definition\ResolveInfo;
use Kepawni\Serge\Infrastructure\GraphQL\TypeResolver;
use PDO;

class AgreementByIdResolver extends TypeResolver
{
    public function __construct(TypeResolver $base = null)
    {
        parent::__construct($base);
        // BASE RESOLVER
        $this->addResolverForField('CqrsQuery', 'getAgreementById','getAggregateId');
        // RESOLVER FROM AGREEMENT->PROPOSAL
        $this->addResolverForField('Agreement', 'proposal','getProposal');
        // RESOLVER FROM AGREEMENT->SENTON
        $this->addResolverForField('Agreement', 'sentOn','getSentOn');
        // RESOLVER FROM AGREEMENT->ISSENT
        $this->addResolverForField('Agreement', 'isSent','getIsSent');
        // RESOLVER FROM AGREEMENT->CONFIRMEDON
        $this->addResolverForField('Agreement', 'confirmedOn','getConfirmedOn');
        // RESOLVER FROM AGREEMENT->ISCONFIRMED
        $this->addResolverForField('Agreement', 'isConfirmed','getIsConfirmed');

        // RESOLVER FOR PROPOSAL->EMAIL
        $this->addResolverForField('Proposal', 'email','getEmailId');
        // RESOLVER FOR PROPOSAL->PROPOSALHASH
        $this->addResolverForField('Proposal', 'proposalHash','getProposal');
        // RESOLVER FOR PROPOSAL->FILE
        $this->addResolverForField('Proposal', 'file','getFile');
        // RESOLVER FOR PROPOSAL->INFO
        $this->addResolverForField('Proposal', 'info', 'getInfo');
        // RESOLVER FOR INFO->ATTRIBUTE
        $this->addResolverForField('KeyValuePair', 'attribute', 'getAttribute');
        // RESOLVER FOR INFO->VALUE
        $this->addResolverForField('KeyValuePair', 'value', 'getValue');

    }

}