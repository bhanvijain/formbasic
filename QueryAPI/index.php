<?php declare(strict_types=1);

/**
 * Usage, when called from command line:
 *     php index.php [ REQUEST.graphql [ VARIABLES.json ] ]
 * REQUEST.graphql
 *     A file containing a GraphQL query or mutation request. If omitted, the
 *     request will default to querying the status.
 * VARIABLES.json
 *     An optional file holding a JSON-encoded hashmap of variable definitions.
 */

use BhanviJain\OnlineAgreementQueryAPI\Resolvers\AgreementByIdResolver;
use BhanviJain\OnlineAgreementQueryAPI\Resolvers\AgreementsByEmailIdResolver;
use BhanviJain\OnlineAgreementQueryAPI\Resolvers\IsProposalConfirmedResolver;
use BhanviJain\OnlineAgreementQueryAPI\Resolvers\ProposalByIdResolver;
use BhanviJain\OnlineAgreementQueryAPI\Resolvers\ProposalsByEmailIdResolver;
use BhanviJain\OnlineAgreementQueryAPI\Resolvers\StatusResolver;
use GraphQL\Server\OperationParams;
use GraphQL\Server\StandardServer;
use Kepawni\Serge\Infrastructure\GraphQL\CustomizedGraphqlServerConfig;
use Kepawni\Serge\Infrastructure\GraphQL\SchemaFileCache;
use Kepawni\Serge\Infrastructure\GraphQL\TypeResolver;

if (PHP_SAPI !== 'cli') {
    header('Access-Control-Allow-Origin: *');
    if (($_SERVER['REQUEST_METHOD'] ?? null) == 'OPTIONS') {
        header('Access-Control-Allow-Headers: content-type');
        header('Access-Control-Allow-Methods: POST');
        exit;
    }
}
require_once __DIR__ . '/vendor/autoload.php';
require_once  __DIR__.'/lib/CallablesForResolvers.php';
$schemaCache = __DIR__ . '/lib/graphqls-cache/';
$schemaFile = __DIR__ . '/query.graphqls';
$db_path = __DIR__ . "/../database.sqlite";

$db = new PDO("sqlite:" . realpath($db_path), null, null,
    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

$context["db"] = $db; // context available to all resolver
$rootValue = null;
$serverConfig = null;
$typeResolver = new TypeResolver();
$typeResolver = new StatusResolver($typeResolver);
$typeResolver = new ProposalByIdResolver($typeResolver);
$typeResolver = new ProposalsByEmailIdResolver($typeResolver);
$typeResolver = new IsProposalConfirmedResolver($typeResolver);
$typeResolver = new AgreementByIdResolver($typeResolver);
$typeResolver = new AgreementsByEmailIdResolver($typeResolver);

try {
    $schemaFileCache = new SchemaFileCache($schemaCache);
    $schema = $schemaFileCache->loadCacheForFile($schemaFile, $typeResolver->generateTypeConfigDecorator());
    $serverConfig = new CustomizedGraphqlServerConfig($schema, $context, $rootValue);
    $standardServer = new StandardServer($serverConfig);
    if (PHP_SAPI === 'cli') {
        $query = isset($argv[1]) ? file_get_contents($argv[1]) : 'query { status }';
        $variables = isset($argv[2]) ? file_get_contents($argv[2]) : '{}';
        echo json_encode(
            $standardServer->executeRequest(
                OperationParams::create(
                    ['query' => $query, 'variables' => $variables],
                    substr(ltrim($query), 0, 8) !== 'mutation'
                )
            ),
            JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
        );
    } else {
        $standardServer->handleRequest();
    }
} catch (Throwable $e) {
    StandardServer::send500Error(
        $serverConfig
            ? new Exception(json_encode($serverConfig->formatError($e), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES))
            : $e,
        true
    );
}
