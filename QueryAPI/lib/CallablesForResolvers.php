<?php

use GraphQL\Type\Definition\ResolveInfo;


function getAggregateIdsFromEmailId ($typeValue, array $args, $context, ResolveInfo $info) {
    /** @var PDO $db */
    $db = $context['db'];
    $emailId = $args['emailId'];
    $query = 'SELECT ProposalID FROM Agreement WHERE EmailID = :emailId';
    $stmt = $db->prepare($query);
    $stmt->execute(['emailId' => $emailId]);
    return $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
}

function getAggregateId ($typeValue, array $args, $context, ResolveInfo $info) {
    /* @var PDO $db */
    $db = $context['db'];
    $aggregateId = $args['id'];
    $query = 'SELECT COUNT(*) FROM Agreement WHERE ProposalID = :proposalId';
    $stmt = $db->prepare($query);
    $stmt->execute(['proposalId' => $aggregateId]);
    return $stmt->fetchColumn() == 1 ? $aggregateId : null;
}

function getProposal($proposalId, array $args, $context, ResolveInfo $info) {
    return $proposalId;
}

function getEmailId($proposalId, array $args, $context, ResolveInfo $info) {
    // first arg is what previous resolver returned
    /* @var PDO $db */
    $db = $context['db'];
    $query = 'SELECT EmailId FROM Agreement WHERE ProposalID = :proposalId';
    $stmt = $db->prepare($query);
    $stmt->execute(['proposalId' => $proposalId]);
    return $stmt->fetchColumn();
}

function getFile($proposalId, array $args, $context, ResolveInfo $info) {
    // first arg is what previous resolver returned
    /* @var PDO $db */
    $db = $context['db'];
    $query = <<<'sql'
                    SELECT 
                        FileID as fileDataUrl,  
                        Name as fileName,
                        Type as fileType
                    FROM Files WHERE ProposalID = :proposalId
                sql;
    $stmt = $db->prepare($query);
    $stmt->execute(['proposalId' => $proposalId]);

    return $stmt->fetchObject();
}

function getInfo($proposalId, array $args, $context, ResolveInfo $info)
{
    // first arg is what previous resolver returned
    /* @var PDO $db */
    $db = $context['db'];
    $query = 'SELECT Info FROM Agreement WHERE ProposalID = :proposalId';
    $stmt = $db->prepare($query);
    $stmt->execute(['proposalId' => $proposalId]);
    $map = json_decode(($stmt->fetchColumn()), true);
    return array_map(function ($key, $val) {
        return [$key, $val];
    }, array_keys($map), array_values($map)) ?: [];
}

function getAttribute($pair, array $args, $context, ResolveInfo $info)
{
    return $pair[0];
}

function getValue($pair, array $args, $context, ResolveInfo $info)
{
    return $pair[1];
}


function getSentOn($proposalId, array $args, $context, ResolveInfo $info) {
    /* @var PDO $db */
    $db = $context['db'];
    $query = 'SELECT SentOn FROM Agreement WHERE ProposalID = :proposalId';
    $stmt = $db->prepare($query);
    $stmt->execute(['proposalId' => $proposalId]);
    return $stmt->fetchColumn();
}

function getIsSent ($proposalId, array $args, $context, ResolveInfo $info) {
    /* @var PDO $db */
    $db = $context['db'];
    $query = 'SELECT SentOn FROM Agreement WHERE ProposalID = :proposalId';
    $stmt = $db->prepare($query);
    $stmt->execute(['proposalId' => $proposalId]);
    return $stmt->fetchColumn()? true:false;
}

function getConfirmedOn($proposalId, array $args, $context, ResolveInfo $info) {
    /* @var PDO $db */
    $db = $context['db'];
    $query = 'SELECT ConfirmedOn FROM Agreement WHERE ProposalID = :proposalId';
    $stmt = $db->prepare($query);
    $stmt->execute(['proposalId' => $proposalId]);
    return $stmt->fetchColumn();
}

function getIsConfirmed($proposalId, array $args, $context, ResolveInfo $info) {
    /* @var PDO $db */
    $db = $context['db'];
    $query = 'SELECT ConfirmedOn FROM Agreement WHERE ProposalID = :proposalId';
    $stmt = $db->prepare($query);
    $stmt->execute(['proposalId' => $proposalId]);
    return $stmt->fetchColumn()? true:false;
}
