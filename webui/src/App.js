import React, {useState} from 'react';
import './App.css';
import {useMutation, useQuery} from "@apollo/react-hooks";
import {queries} from "./queries";
import {v4} from "uuid";
import AttributeEditor from "./AttributeEditor";

function App() {
    // states
    const [emailId, setEmailId] = useState('');
    const [attributeList, setAttributeList] = useState([{attribute: "one", value: "val1"}]);
    const [file, setFile] = useState("");
    const [proposalId, setProposalId] = useState("");

    // queries
    const {data, error, loading} = useQuery(queries.getAgreementsByEmailId,
        {
            skip: !emailId,
            variables: {email: emailId}
        }
    );

    // mutations
    const [establishProposal, {error: establishProposalError}] = useMutation(queries.establishProposal);
    const [attachFile, {error: attachFileError}] = useMutation(queries.attachFile);
    const [provideInfo, {error: provideInfoError}] = useMutation(queries.provideInfo);
    const [sendProposal, {error: sendError}] = useMutation(queries.sendProposal);

    // onChange handlers
    const handleCreateProposal = () => {
        const id = v4();
        setProposalId(id);
        establishProposal({variables: {proposalId: id, emailId}});
    }

    const handleFileUpload = event => {
        const uploadedFile = event.target.files[0];
        const reader = new FileReader();
        reader.addEventListener("load", event => {
            const dataUrl = event.target.result;
            attachFile({
                variables: {
                    proposalId,
                    fileDataUrl: dataUrl,
                    fileName: uploadedFile.name,
                    fileType: uploadedFile.type
                }
            });
            setFile(uploadedFile);
        });
        reader.readAsDataURL(uploadedFile);
    }

    const handleAttributes = (attributeList) => {
        setAttributeList(attributeList);
        attributeList.map(
            ({attribute: a, value: v}) =>
                provideInfo({
                    variables:
                        {proposalId, attr: a, val: v}
                })
        )
    };

    const handleSend = () => {
        sendProposal({
            variables: {proposalId}
        });
        setProposalId("");
    }

    return (

        proposalId ?
            <div>
                <div>
                    <input name={'attachment'} id={'attachment'} type={'file'} onChange={handleFileUpload}/>
                </div>
                <div>
                    <AttributeEditor attributes={attributeList} onChange={handleAttributes}/>
                </div>
                <div>
                    <br/>
                    <button onClick={handleSend}>Send</button>
                </div>

            </div>
            :
            <div>
                <div><input value={emailId} onChange={event => setEmailId(event.target.value)}/></div>
                {
                    loading ? 'loading...' : (error ? 'error:' + error :
                            data && data.getAgreementsByEmailId.map(
                                agreement =>
                                    <div>
                                        {agreement.proposal.proposalHash}
                                    </div>
                            )
                    )
                }
                <button onClick={handleCreateProposal}>Create New Proposal</button>
            </div>

    );
}

export default App;
