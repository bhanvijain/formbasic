import React, {useState} from 'react';

function AttributeEditor({attributes, onChange}) {
    const [newAttribute, setNewAttribute] = useState("");
    const [newValue, setNewValue] = useState("");

    const handleNewAttribute = event => {
        setNewAttribute(event.target.value);
    };
    const handleNewValue = event => {
        setNewValue(event.target.value);
    };
    const handleAddInfo = () => {
        onChange([...attributes.filter(({attribute}) => attribute !== newAttribute),
            {attribute: newAttribute, value: newValue}]);
        setNewValue("");
        setNewAttribute("");
    };
    return (
        <div>
            {
                attributes.map(
                    ({attribute, value}) => <div>
                        <input
                            onChange={
                                event => {
                                    onChange([...attributes.map((pair) => pair.attribute === attribute ? {
                                        attribute: event.target.value,
                                        value: pair.value
                                    } : pair)])
                                }
                            }
                            value={attribute}
                        />
                        :
                        <input
                            onChange={
                                event => {
                                    onChange([...attributes.map((pair) => pair.attribute === attribute ? {
                                        attribute: pair.attribute,
                                        value: event.target.value
                                    } : pair)])
                                }
                            }
                            value={value}
                        />
                    </div>
                )
            }
            <div>
                <br/>
                <input
                    value={newAttribute}
                    onChange={handleNewAttribute}
                />
                :
                <input
                    value={newValue}
                    onChange={handleNewValue}
                />
                <button onClick={handleAddInfo}>Add Info</button>
            </div>
        </div>

    );
}


export default AttributeEditor;