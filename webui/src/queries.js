import gql from "graphql-tag";

export const queries = {
    getAgreementsByEmailId: gql`
        query ($email: String!){
            getAgreementsByEmailId(emailId: $email){
                proposal{proposalHash}
                sentOn
                isConfirmed
            }
        }
    `,

    establishProposal: gql`mutation(
        $proposalId: ID!,
        $emailId:String!) {
        Proposal(id: $proposalId) {
            establish(emailId: $emailId )
        }
    }`,

    attachFile: gql`mutation(
        $proposalId: ID!,
        $fileDataUrl: String!,
        $fileName: String!,
        $fileType: String!) {
        Proposal(id: $proposalId) {
            attachFile(file: {
                fileDataUrl: $fileDataUrl
                fileName: $fileName
                fileType: $fileType
            })
        }
    }`,
    provideInfo: gql`mutation(
        $proposalId: ID!,
        $attr: String!,
        $val: String!) {
        Proposal(id: $proposalId) {
            provideInfo(attribute: {attribute: $attr value: $val})
        }
    }`,

    sendProposal: gql`mutation ($proposalId: ID!){
        Proposal(id: $proposalId) {
            send
        }
    }`
};