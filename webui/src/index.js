import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {InMemoryCache} from "apollo-cache-inmemory";
import {ApolloClient} from "apollo-client";
import {ApolloLink} from "apollo-link";
import {ApolloProvider} from '@apollo/react-hooks';
import {onError} from "apollo-link-error";
import {HttpLink} from "apollo-link-http";


const commandApiUri = 'http://localhost:9000';
const queryApiUri = 'http://localhost:9090';
const cache = new InMemoryCache();

function fetchWithAltUriForMutations(alternativeUri) {
    return (uri, options) => fetch(
        JSON.parse(options.body).query.match(/^\s*mutation/)
            ? alternativeUri
            : uri,
        options
    );
}

const client = new ApolloClient({
    cache,
    link: ApolloLink.from([
        onError(({graphQLErrors, networkError}) => {
            if (graphQLErrors)
                graphQLErrors.forEach(({message, locations, path}) =>
                    console.log(
                        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
                    ),
                );
            if (networkError) console.log('[Network error]:', networkError);
        }),
        new HttpLink({
            uri: queryApiUri,
            fetch: fetchWithAltUriForMutations(commandApiUri)
        })

    ])
});

ReactDOM.render(
    <React.StrictMode>
        <ApolloProvider client={client}>
            <App/>
        </ApolloProvider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
