<?php
$content = ["operationName" => null,
    "variables" => ["proposalId" => $_GET["proposal"]],
    "query" => <<<'q'
    mutation ($proposalId: ID!) {
        Proposal(id :$proposalId) {
            confirm
        }
    }
    q
];
$opts = array(
    'http' => array(
        'method' => "POST",
        'content' => json_encode($content),
        'header' => ["content-type: application/json"],
        'ignore_errors' => true
    )
);
$context = stream_context_create($opts);
$result = json_decode(file_get_contents("http://localhost:9000/", false, $context), true);
if (isset($result["errors"])) {
    $message = $result["errors"][0]["message"];
} else {
    $message = "Thank You. Confirmation recorded";
}
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>CONFIRMED</title>
</head>
<body>
<h4><?php echo $message ?></h4>
</body>

</html>