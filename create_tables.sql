CREATE TABLE Agreement (
                           "ProposalID"	varchar(36) NOT NULL UNIQUE,
                           "EmailID"	varchar(100) NOT NULL,
                           "Info"	TEXT,
                           "SentOn"	DATETIME,
                           "ConfirmedOn"	DATETIME,
                           PRIMARY KEY("ProposalID")
);
CREATE TABLE Files (
                       "ProposalID" varchar(36) NOT NULL,
                       "FileID"	varchar(36) NOT NULL ,
                       "Name"	varchar(80) NOT NULL,
                       "Type"	varchar(80) NOT NULL,
                       FOREIGN KEY ("ProposalID") REFERENCES Agreement(ProposalID),
                       PRIMARY KEY("ProposalID","FileID")
);
SELECT * FROM sqlite_master;
Alter Table Agreement
ADD COLUMN "ProposalHash" varchar(50);