<?php declare(strict_types=1);

/**
 * Usage, when called from command line:
 *     php index.php [ REQUEST.graphql [ VARIABLES.json ] ]
 * REQUEST.graphql
 *     A file containing a GraphQL query or mutation request. If omitted, the
 *     request will default to querying the status.
 * VARIABLES.json
 *     An optional file holding a JSON-encoded hashmap of variable definitions.
 */

use GraphQL\Server\OperationParams;
use GraphQL\Server\StandardServer;
use Kepawni\Serge\Infrastructure\GraphQL\CqrsCommandBus;
use Kepawni\Serge\Infrastructure\GraphQL\CustomizedGraphqlServerConfig;
use Kepawni\Serge\Infrastructure\GraphQL\SchemaFileCache;
use Kepawni\Serge\Infrastructure\GraphQL\TypeResolver;
use PHPMailer\PHPMailer\PHPMailer;


if (PHP_SAPI !== 'cli') {
    header('Access-Control-Allow-Origin: *');
    if (($_SERVER['REQUEST_METHOD'] ?? null) == 'OPTIONS') {
        header('Access-Control-Allow-Headers: content-type');
        header('Access-Control-Allow-Methods: POST');
        exit;
    }
}
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/lib/command-bus.inc.php';
require_once __DIR__ . '/lib/useConfig.php';
$schemaCache = __DIR__ . '/lib/graphqls-cache/';
$schemaFile = __DIR__ . '/command.graphqls';

$mailer = new PHPMailer();
$mailer->isSMTP();
$mailer->Host = SMTP_HOST;
$mailer->SMTPAuth = true;
$mailer->Username = SMTP_USER;
$mailer->Password = SMTP_PASSWORD;
if (SMTP_SSL)
    $mailer->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
elseif (SMTP_TLS)
    $mailer->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
$mailer->Port = SMTP_PORT;
$mailer->setFrom(FROM_ADDRESS);
$context = [
    "mailer" => $mailer,
    "baseURL" => BASE_URL
];
$rootValue = null;
$serverConfig = null;
$typeResolver = new TypeResolver();
$typeResolver->addResolverForField('CqrsQuery', 'status', function () {
    return true;
});
$commandBus = new CqrsCommandBus('CqrsAggregateMutators', $typeResolver);
addCommandHandlersToCommandBus($commandBus);
try {
    $schemaFileCache = new SchemaFileCache($schemaCache);
    $schema = $schemaFileCache->loadCacheForFile($schemaFile, $commandBus->generateTypeConfigDecorator());
    $serverConfig = new CustomizedGraphqlServerConfig($schema, $context, $rootValue);
    $standardServer = new StandardServer($serverConfig);
    if (PHP_SAPI === 'cli') {
        $query = isset($argv[1]) ? file_get_contents($argv[1]) : 'query { status }';
        $variables = isset($argv[2]) ? file_get_contents($argv[2]) : '{}';
        echo json_encode(
            $standardServer->executeRequest(
                OperationParams::create(
                    ['query' => $query, 'variables' => $variables],
                    substr(ltrim($query), 0, 8) !== 'mutation'
                )
            ),
            JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
        );
    } else {
        $standardServer->handleRequest();
    }
} catch (Throwable $e) {
    StandardServer::send500Error(
        $serverConfig
            ? new Exception(json_encode($serverConfig->formatError($e), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES))
            : $e,
        true
    );
}
