<?php declare(strict_types=1);
namespace BhanviJain\OnlineAgreementCommandAPI\Value;

use Kepawni\Serge\Infrastructure\AbstractValueObjectBase;
use Kepawni\Twilted\Windable;

/**
 * @property-read string $attribute
 * @property-read string $value
 * @method self withAttribute(string $v)
 * @method self withValue(string $v)
 */
class KeyValuePair extends AbstractValueObjectBase
{
    /**
 * @param array $map
 * @return static
 */
public static function fromHashMap(array $map): AbstractValueObjectBase
    {
        return new self(
            strval($map['attribute']),
            strval($map['value'])
        );
    }

/**
 * @param array $spool
 * @return static
 */
public static function unwind(array $spool): Windable
    {
        return new self(
            strval($spool[0]),
            strval($spool[1])
        );
    }

public function __construct(string $attribute, string $value)
    {
        $this->init('attribute', $attribute);
        $this->init('value', $value);
    }

public function windUp(): array
    {
        return [
            $this->attribute,
            $this->value
        ];
    }
}
