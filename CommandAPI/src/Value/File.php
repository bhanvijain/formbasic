<?php declare(strict_types=1);
namespace BhanviJain\OnlineAgreementCommandAPI\Value;

use Kepawni\Serge\Infrastructure\AbstractValueObjectBase;
use Kepawni\Twilted\Windable;

/**
 * @property-read string $fileDataUrl
 * @property-read string $fileName
 * @property-read string $fileType
 * @method self withFileDataUrl(string $v)
 * @method self withFileName(string $v)
 * @method self withFileType(string $v)
 */
class File extends AbstractValueObjectBase
{
    /**
 * @param array $map
 * @return static
 */
public static function fromHashMap(array $map): AbstractValueObjectBase
    {
        return new self(
            strval($map['fileDataUrl']),
            strval($map['fileName']),
            strval($map['fileType'])
        );
    }

/**
 * @param array $spool
 * @return static
 */
public static function unwind(array $spool): Windable
    {
        return new self(
            strval($spool[0]),
            strval($spool[1]),
            strval($spool[2])
        );
    }

public function __construct(string $fileDataUrl, string $fileName, string $fileType)
    {
        $this->init('fileDataUrl', $fileDataUrl);
        $this->init('fileName', $fileName);
        $this->init('fileType', $fileType);
    }

public function windUp(): array
    {
        return [
            $this->fileDataUrl,
            $this->fileName,
            $this->fileType
        ];
    }
}
