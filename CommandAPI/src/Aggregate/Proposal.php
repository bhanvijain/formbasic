<?php declare(strict_types=1);

namespace BhanviJain\OnlineAgreementCommandAPI\Aggregate;

use BhanviJain\OnlineAgreementCommandAPI\EmailStructure;
use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\FileWasAttached;
use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\InfoWasProvided;
use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\ProposalWasConfirmed;
use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\ProposalWasEstablished;
use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\ProposalWasSent;
use BhanviJain\OnlineAgreementCommandAPI\Exceptions\EmailIdErrorException;
use BhanviJain\OnlineAgreementCommandAPI\Exceptions\EmailNotSentException;
use BhanviJain\OnlineAgreementCommandAPI\Exceptions\FileNotAttachedException;
use BhanviJain\OnlineAgreementCommandAPI\Exceptions\ForbiddenFileTypeException;
use BhanviJain\OnlineAgreementCommandAPI\Exceptions\IncorrectDataUrlException;
use BhanviJain\OnlineAgreementCommandAPI\Exceptions\ProposalAlreadyConfirmedException;
use BhanviJain\OnlineAgreementCommandAPI\Exceptions\ProposalNotEstablishedException;
use BhanviJain\OnlineAgreementCommandAPI\Exceptions\SentAlreadyException;
use BhanviJain\OnlineAgreementCommandAPI\Value\File;
use BhanviJain\OnlineAgreementCommandAPI\Value\KeyValuePair;
use Kepawni\Twilted\Basic\SimpleAggregateRoot;
use Kepawni\Twilted\EntityIdentifier;
use PHPMailer\PHPMailer\PHPMailer;

class Proposal extends SimpleAggregateRoot
{
    private bool $hasBeenSentAlready = false;
    private bool $isFileAttached = false;
    private bool $isEstablished = false;
    private bool $isConfirmed = false;
    private string $emailId = "";
    private string $fileDataUrl;
    private string $fileType;
    private string $fileName;
    private array $info = [];

    public static function establish(EntityIdentifier $proposalId, string $emailId): self
    {
        self::guardEmailIdIsSyntacticallyCorrect($emailId);
        $newProposal = new static($proposalId);
        $newProposal->recordThat(
            new ProposalWasEstablished($emailId)
        );
        return $newProposal;
    }

    public function attachFile(File $file): void
    {
        $this->guardProposalEstablished();
        $this->guardIsAlreadySent();
        $this->guardCheckDataUrl($file->fileDataUrl);
        $this->guardFileIsAllowed($file);
        $this->recordThat(new FileWasAttached($file));
    }

    public function provideInfo(KeyValuePair $attribute): void
    {
        // $this->guardAttributeIsSupported($attribute);
        $this->guardProposalEstablished();
        $this->guardIsAlreadySent();
        $this->recordThat(new InfoWasProvided($attribute));
    }

    public function send(PHPMailer $mailer, $baseUrl): void
    {
        $this->guardProposalEstablished();
        $this->guardFileIsAttached();
        $this->guardIsAlreadySent();

        // email creation
        $mailer->addAddress($this->emailId);
        $proposalId = $this->getId()->fold();
        $body = new EmailStructure("CONFIRMATION LINK", true,
            $baseUrl, $proposalId, $this->info);
        $mailer->Subject = $body->subject;

        //email attachment
        $data = explode(";base64,", $this->fileDataUrl)[1];
        $mailer->AddStringAttachment(base64_decode($data), $this->fileName,
            $encoding = PHPMailer::ENCODING_BASE64, $type = $this->fileType);
        //email body
        $mailer->isHTML($body->isHtml);
        $mailer->Body = $body->message;

        if ($mailer->send()) {
            $this->recordThat(new ProposalWasSent());
        } else
            throw new EmailNotSentException('Email was not sent');
    }

    public function confirm(): void
    {
        $this->guardProposalEstablished(); // check if the proposal exists
        $this->guardIsAlreadyConfirmed();
        $this->recordThat(new ProposalWasConfirmed());
    }

    protected function whenFileWasAttached(FileWasAttached $event): void
    {
        $this->isFileAttached = true;
        $this->fileDataUrl = $event->file->fileDataUrl;
        $this->fileType = $event->file->fileType;
        $this->fileName = $event->file->fileName;

    }

    protected function whenInfoWasProvided(InfoWasProvided $event): void
    {
        $this->info[$event->attribute->attribute] = $event->attribute->value;
    }

    protected function whenProposalWasEstablished(ProposalWasEstablished $event): void
    {
        $this->isEstablished = true;
        $this->emailId = $event->emailId;
    }

    protected function whenProposalWasConfirmed(ProposalWasConfirmed $event): void
    {
        $this->isConfirmed = true;
    }

    protected function whenProposalWasSent(ProposalWasSent $event): void
    {
        $this->hasBeenSentAlready = true;
    }

    private static function guardEmailIdIsSyntacticallyCorrect(string $emailId)
    {
        if (!filter_var($emailId, FILTER_VALIDATE_EMAIL))
            throw new EmailIdErrorException("Invalid Email ID");
    }

    private function guardProposalEstablished()
    {
        if (!$this->isEstablished)
            throw new ProposalNotEstablishedException("This proposal has not been established.");
    }

    private function guardFileIsAllowed(File $file): void
    {
        $type = $file->fileType;
        $allowedTypes = ["application/pdf", "application/msword", "application/excel",
            "application/vnd.ms-excel", "application/x-excel", "application/x-msexcel", "image/png",
            "image/gif", "image/jpeg", "image/pjpeg", "application/vnd.oasis.opendocument.formula"];
        foreach ($allowedTypes as $t) {
            if ($type == $t)
                return;
        }
        throw new ForbiddenFileTypeException("This file type is not allowed");
    }

    private function guardFileIsAttached(): void
    {
        if (!$this->isFileAttached)
            throw new FileNotAttachedException("No File Attached.");

    }

    private function guardIsAlreadySent()
    {
        if ($this->hasBeenSentAlready)
            throw new SentAlreadyException("This proposal has already been sent");
    }


    private function guardIsAlreadyConfirmed()
    {
        if ($this->isConfirmed)
            throw new ProposalAlreadyConfirmedException("This proposal has already been confirmed");
    }

    private function guardCheckDataUrl(string $fileDataUrl)
    {
        if(substr_count($fileDataUrl,";base64,") != 1)
            throw new IncorrectDataUrlException("The data url is incorrect");
    }
}
