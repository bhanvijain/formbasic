<?php declare(strict_types=1);

namespace BhanviJain\OnlineAgreementCommandAPI\Handler;

use BhanviJain\OnlineAgreementCommandAPI\Aggregate\Proposal;
use BhanviJain\OnlineAgreementCommandAPI\Value\File;
use BhanviJain\OnlineAgreementCommandAPI\Value\KeyValuePair;
use GraphQL\Type\Definition\ResolveInfo;
use Kepawni\Twilted\Basic\AggregateUuid;
use Kepawni\Twilted\Basic\SimpleCommandHandler;

class ProposalHandler extends SimpleCommandHandler
{
    public function attachFile(string $aggregateId, array $methodArgs, $context, ResolveInfo $info): void
    {
        /** @var Proposal $theProposal */
        $theProposal = $this->loadFromRepository(AggregateUuid::unfold($aggregateId));
        $theProposal->attachFile(
            File::fromHashMap($methodArgs['file'])
        );
        $this->saveToRepository($theProposal);
    }

    public function confirm(string $aggregateId, array $methodArgs, $context, ResolveInfo $info): void
    {
        /** @var Proposal $theProposal */
        $theProposal = $this->loadFromRepository(AggregateUuid::unfold($aggregateId));
        $theProposal->confirm();
        $this->saveToRepository($theProposal);
    }

    public function establish(string $aggregateId, array $methodArgs, $context, ResolveInfo $info): void
    {
        /** @var Proposal $newProposal */
        $newProposal = Proposal::establish(
            AggregateUuid::unfold($aggregateId),
            strval($methodArgs['emailId'])
        );
        $this->saveToRepository($newProposal);
    }

    public function provideInfo(string $aggregateId, array $methodArgs, $context, ResolveInfo $info): void
    {
        /** @var Proposal $theProposal */
        $theProposal = $this->loadFromRepository(AggregateUuid::unfold($aggregateId));
        $theProposal->provideInfo(
            KeyValuePair::fromHashMap($methodArgs['attribute'])
        );
        $this->saveToRepository($theProposal);
    }

    public function send(string $aggregateId, array $methodArgs, $context, ResolveInfo $info): void
    {
        /** @var Proposal $theProposal */
        $theProposal = $this->loadFromRepository(AggregateUuid::unfold($aggregateId));
        $theProposal->send($context["mailer"], $context["baseURL"]);
        $this->saveToRepository($theProposal);
    }
}
