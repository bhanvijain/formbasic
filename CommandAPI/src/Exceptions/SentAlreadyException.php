<?php declare(strict_types=1);
namespace BhanviJain\OnlineAgreementCommandAPI\Exceptions;

use RuntimeException;

class SentAlreadyException extends RuntimeException
{
}
