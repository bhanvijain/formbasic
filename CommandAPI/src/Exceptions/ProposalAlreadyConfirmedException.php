<?php


namespace BhanviJain\OnlineAgreementCommandAPI\Exceptions;


use RuntimeException;

class ProposalAlreadyConfirmedException extends RuntimeException
{

}