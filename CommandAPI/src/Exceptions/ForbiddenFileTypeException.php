<?php declare(strict_types=1);
namespace BhanviJain\OnlineAgreementCommandAPI\Exceptions;

use RuntimeException;

class ForbiddenFileTypeException extends RuntimeException
{
}
