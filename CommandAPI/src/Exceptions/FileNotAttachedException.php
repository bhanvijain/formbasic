<?php


namespace BhanviJain\OnlineAgreementCommandAPI\Exceptions;


use RuntimeException;

class FileNotAttachedException extends RuntimeException
{

}