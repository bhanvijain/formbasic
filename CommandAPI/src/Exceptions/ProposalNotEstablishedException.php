<?php


namespace BhanviJain\OnlineAgreementCommandAPI\Exceptions;


use RuntimeException;

class ProposalNotEstablishedException extends RuntimeException
{

}