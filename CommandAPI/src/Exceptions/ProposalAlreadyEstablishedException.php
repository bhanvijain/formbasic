<?php


namespace BhanviJain\OnlineAgreementCommandAPI\Exceptions;


use RuntimeException;

class ProposalAlreadyEstablishedException extends RuntimeException
{

}