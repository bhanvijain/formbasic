<?php declare(strict_types=1);

namespace BhanviJain\OnlineAgreementCommandAPI\Projector;

use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\FileWasAttached;
use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\InfoWasProvided;
use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\ProposalWasConfirmed;
use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\ProposalWasEstablished;
use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\ProposalWasSent;
use BhanviJain\OnlineAgreementCommandAPI\Exceptions\ProposalAlreadyEstablishedException;
use Kepawni\Twilted\DomainEvent;
use PDO;
use PDOException;
use function Ramsey\Uuid\v5;


class ProposalProjector
{

    private PDO $db;

    public function __construct($dbConnection)
    {
        // set up the instance
        $this->db = $dbConnection;
    }

    public function whenProposalWasEstablished(ProposalWasEstablished $payload, DomainEvent $event): void
    {
        $aggregateId = $event->getId()->fold();
        $emailId = $payload->emailId;
        $query = 'INSERT INTO Agreement(ProposalID, EmailID) VALUES (:proposalId,:emailId);';
        $stmt = $this->db->prepare($query);
        try {
            $stmt->execute(['proposalId' => $aggregateId, 'emailId' => $emailId]);
        } catch (PDOException $exception) {
            throw new ProposalAlreadyEstablishedException("This proposal is already established");
        }
    }

    public function whenFileWasAttached(FileWasAttached $payload, DomainEvent $event): void
    {
        $aggregateId = $event->getId()->fold();
        $fileDataUrl = $payload->file->fileDataUrl;
        $fileName = $payload->file->fileName;
        $fileType = $payload->file->fileType;

        $query = 'SELECT COUNT(ProposalID) FROM Files WHERE ProposalID = :proposalid;';
        $stmt = $this->db->prepare($query);
        $stmt->execute(['proposalid' => $aggregateId]);
        if ($stmt->fetchColumn() == 0) {
            $query = 'INSERT INTO Files VALUES (:proposalid,:fid,:fname,:ftype);';
            $stmt = $this->db->prepare($query);
            $stmt->execute(['proposalid' => $aggregateId, 'fid' => $fileDataUrl, 'fname' => $fileName, 'ftype' => $fileType]);
        } else {
            $query = 'UPDATE Files SET FileID = :fid, Name = :fname, Type = :ftype WHERE ProposalID = :proposalid;';
            $stmt = $this->db->prepare($query);
            $stmt->execute(['proposalid' => $aggregateId, 'fid' => $fileDataUrl, 'fname' => $fileName, 'ftype' => $fileType]);
        }
        // generate proposal hash
        $query = 'SELECT EmailID,Info FROM Agreement WHERE ProposalID = :proposalId';
        $stmt = $this->db->prepare($query);
        $stmt->execute(['proposalId' => $aggregateId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $info = $row["Info"] ?: '[]';
        $emailId = $row["EmailID"];
        $proposalHash = v5(
            v5(
                v5(ROOTUUID, $emailId),
                base64_decode(explode(';base64,', $fileDataUrl)[1]
                )
            ), $info);

        $query = 'UPDATE Agreement SET ProposalHash = :proposalHash WHERE ProposalID = :proposalId';
        $stmt = $this->db->prepare($query);
        $stmt->execute(['proposalId' => $aggregateId, 'proposalHash' => $proposalHash]);
    }

    public function whenInfoWasProvided(InfoWasProvided $payload, DomainEvent $event): void
    {
        $aggregateId = $event->getId()->fold();
        $query = 'SELECT Info FROM Agreement WHERE ProposalID = :proposalId';
        $stmt = $this->db->prepare($query);
        $stmt->execute(['proposalId' => $aggregateId]);

        $info = json_decode($stmt->fetchColumn() ?: "[]", true);
        $info[$payload->attribute->attribute] = $payload->attribute->value;
        ksort($info);
        $info = json_encode($info);

        $query = 'UPDATE Agreement SET Info = :info WHERE ProposalID = :proposal;';
        $stmt = $this->db->prepare($query);
        $stmt->execute(['proposal' => $aggregateId, 'info' => $info]);

        // generate proposal hash
        $query = 'SELECT FileID FROM Files WHERE ProposalID = :proposalId';
        $stmt = $this->db->prepare($query);
        $stmt->execute(['proposalId' => $aggregateId]);
        $fileDataUrl = $stmt->fetchColumn() ?: '';

        $query = 'SELECT EmailID FROM Agreement WHERE ProposalID = :proposalId';
        $stmt = $this->db->prepare($query);
        $stmt->execute(['proposalId' => $aggregateId]);
        $emailId = $stmt->fetchColumn() ?: '';

        $proposalHash = v5(v5(v5(ROOTUUID, $emailId), $fileDataUrl), $info);
        $query = 'UPDATE Agreement SET ProposalHash = :proposalHash WHERE ProposalID = :proposalId';
        $stmt = $this->db->prepare($query);
        $stmt->execute(['proposalId' => $aggregateId, 'proposalHash' => $proposalHash]);
    }

    public function whenProposalWasSent(ProposalWasSent $payload, DomainEvent $event): void
    {
        $aggregateId = $event->getId()->fold();
        $sentOn = $event->getRecordedOn()->format(DATE_ATOM);

        $query = 'UPDATE Agreement SET SentOn = :sentOn WHERE ProposalID = :proposalID;';
        $stmt = $this->db->prepare($query);
        $stmt->execute(['proposalID' => $aggregateId, 'sentOn' => $sentOn]);
    }

    public function whenProposalWasConfirmed(ProposalWasConfirmed $payload, DomainEvent $event): void
    {
        $aggregateId = $event->getId()->fold();
        $confirmedOn = $event->getRecordedOn()->format(DATE_ATOM);

        $query = 'UPDATE Agreement SET ConfirmedOn = :confirmedOn WHERE ProposalID = :proposalID;';
        $stmt = $this->db->prepare($query);
        $stmt->execute(['proposalID' => $aggregateId, 'confirmedOn' => $confirmedOn]);

    }
}
