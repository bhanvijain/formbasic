<?php declare(strict_types=1);
namespace BhanviJain\OnlineAgreementCommandAPI;

use Kepawni\Twilted\DomainEvent;
use Kepawni\Twilted\EventBus;
use Kepawni\Twilted\EventStream;

/**
 * A simple bus that accepts handlers with when*SomethingHasHappened*()-style methods that will be invoked with the
 * event payload (e. g. SomethingHasHappened) as 1st argument and the DomainEvent as the 2nd.
 *
 * If an event occurs that is not covered by any handler, then a message will be sent to STDOUT on shutdown.
 */
class SimpleWhenMethodEventBus implements EventBus
{
    private $handlers = [];

    public function dispatch(EventStream $events): void
    {
        /** @var DomainEvent $domainEvent */
        foreach ($events as $domainEvent) {
            $methodName = 'when' . $this->shortClassName($domainEvent->getPayload());
            $handled = false;
            foreach ($this->handlers as $handler) {
                if (method_exists($handler, $methodName)) {
                    $handler->$methodName($domainEvent->getPayload(), $domainEvent);
                    $handled = true;
                }
            }
            if (!$handled) {
                register_shutdown_function(
                    function () use ($domainEvent) {
                        echo PHP_EOL, 'Unhandled event ', $this->shortClassName($domainEvent->getPayload()), ': ';
                        print_r(
                            [
                                $domainEvent->getRecordedOn()->format(DATE_ATOM),
                                $domainEvent->getId()->fold(),
                                array_map('json_encode', $domainEvent->getPayload()->windUp())
                            ]
                        );
                    }
                );

            }
        }
    }

    public function registerHandler(/*object*/ $handler): self
    {
        $this->handlers[] = $handler;
        return $this;
    }

    private function shortClassName(/*object*/ $object): string
    {
        return array_reverse(explode('\\', get_class($object)))[0];
    }
}
