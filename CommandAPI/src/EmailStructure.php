<?php


namespace BhanviJain\OnlineAgreementCommandAPI;

use Kepawni\Twilted\Basic\ImmutableValue;

/**
 * @property-read bool $isHtml
 * @property-read string $message
 * @property-read string $subject
 */
class EmailStructure extends ImmutableValue
{

    /**
     * EmailStructure constructor.
     * @param string $subject
     * @param bool $isHtml
     * @param string $baseUrl
     * @param string $proposal
     * @param array $additionalInfo
     */

    function __construct(string $subject,bool $isHtml, string $baseUrl, string $proposal, array $additionalInfo)
    {
        $this->init('isHtml', $isHtml);
        $this->init('subject',$subject);
        if ($isHtml) {
            $msg = "<p>Hello,<br><br>someone requests your approval to the attached document and these<br>
            additional details:<br><br>";
            foreach ($additionalInfo as $item => $value) {
                $msg .= $item . " : " . $value . "<br>";
            }
            $msg .= "</p><p><h4>If you have read the attached document and agree to it, please click on the link below</h4>
                   <a href=\"" . $baseUrl . "/confirm.php?proposal=" . $proposal . "\">I AGREE</a></p>";
        } else {
            $msg = "Hello,\n\nsomeone requests your approval to the attached document and these\n
            additional details:\n\n";
            foreach ($additionalInfo as $item => $value) {
                $msg .= $item . " : " . $value . "\n";
            }
            $msg .= "If you have read the attached document and agree to it, 
            please copy the link below into the browser\n" . $baseUrl . "/confirm.php?proposal=" . $proposal;
        }
        $this->init('message', $msg);
    }

}