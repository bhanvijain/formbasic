<?php declare(strict_types=1);
namespace BhanviJain\OnlineAgreementCommandAPI\Event\Proposal;

use Kepawni\Serge\Infrastructure\AbstractEventPayloadBase;
use Kepawni\Twilted\Windable;

class ProposalWasConfirmed extends AbstractEventPayloadBase
{
    /**
 * @param array $spool
 * @return static
 */
public static function unwind(array $spool): Windable
    {
        return new self(
            
        );
    }

public function __construct()
    {
        
    }

public function windUp(): array
    {
        return [
            
        ];
    }
}
