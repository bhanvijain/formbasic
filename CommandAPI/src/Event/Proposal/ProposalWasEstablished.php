<?php declare(strict_types=1);
namespace BhanviJain\OnlineAgreementCommandAPI\Event\Proposal;

use Kepawni\Serge\Infrastructure\AbstractEventPayloadBase;
use Kepawni\Twilted\Windable;

/**
 * @property-read string $emailId
 */
class ProposalWasEstablished extends AbstractEventPayloadBase
{
    /**
 * @param array $spool
 * @return static
 */
public static function unwind(array $spool): Windable
    {
        return new self(
            strval($spool[0])
        );
    }

public function __construct(string $emailId)
    {
        $this->init('emailId', $emailId);
    }

public function windUp(): array
    {
        return [
            $this->emailId
        ];
    }
}
