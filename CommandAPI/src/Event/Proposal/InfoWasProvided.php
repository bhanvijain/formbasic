<?php declare(strict_types=1);
namespace BhanviJain\OnlineAgreementCommandAPI\Event\Proposal;
use BhanviJain\OnlineAgreementCommandAPI\Value\KeyValuePair;
use Kepawni\Serge\Infrastructure\AbstractEventPayloadBase;
use Kepawni\Twilted\Windable;

/**
 * @property-read KeyValuePair $attribute
 */
class InfoWasProvided extends AbstractEventPayloadBase
{
    /**
     * @param array $spool
     * @return static
     */
    public static function unwind(array $spool): Windable
    {
        return new self(
            KeyValuePair::unwind($spool[0])
        );
    }
    
    public function __construct(KeyValuePair $attribute)
    {
        $this->init('attribute', $attribute);
    }
    
    public function windUp(): array
    {
        return [
            $this->attribute->windUp()
        ];
    }
}
