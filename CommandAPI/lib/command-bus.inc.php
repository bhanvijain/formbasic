<?php declare(strict_types=1);

use BhanviJain\OnlineAgreementCommandAPI\Aggregate\Proposal;
use BhanviJain\OnlineAgreementCommandAPI\Handler\ProposalHandler;
use BhanviJain\OnlineAgreementCommandAPI\JsonEventStore;
use BhanviJain\OnlineAgreementCommandAPI\Projector\ProposalProjector;
use BhanviJain\OnlineAgreementCommandAPI\SimpleWhenMethodEventBus;
use Kepawni\Serge\Infrastructure\GraphQL\CqrsCommandBus;
use Kepawni\Twilted\Basic\SimpleRepository;
use Kepawni\Twilted\EventBus;

function addCommandHandlersToCommandBus(CqrsCommandBus $commandBus): void
{
    $eventStore = new JsonEventStore();
    $eventBus = addProjectorsToEventBus(new SimpleWhenMethodEventBus());
    $proposalRepository = new SimpleRepository(Proposal::class, $eventBus, $eventStore);
    // call append to add all your command handlers
    $commandBus->append(Proposal::class, new ProposalHandler($proposalRepository));
}

function addProjectorsToEventBus(SimpleWhenMethodEventBus $eventBus): EventBus
{
    $db_path = __DIR__ . "/../../database.sqlite";
    // call registerHandler to add all your projectors
    $db = new PDO("sqlite:" .realpath($db_path), null, null,
        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    $eventBus->registerHandler(new ProposalProjector($db));
    return $eventBus;
}

