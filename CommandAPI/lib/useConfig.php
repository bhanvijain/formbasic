<?php declare(strict_types=1);
require_once is_file(__DIR__ . '/../config.private.php')
    ? __DIR__ . '/../config.private.php'
    : __DIR__ . '/../config.fallback.php';
