<?php declare(strict_types=1);

const DATABASE = "database.sqlite";
const ROOTUUID = "25366db3-e466-4de4-a7d4-e6d4e8e77aa1";
const BASE_URL = "http://localhost:8000";
const FROM_ADDRESS='john_doe@example.com';
const SMTP_HOST='smtp.example.com';
const SMTP_PORT=465;
const SMTP_USER='john_doe';
const SMTP_PASSWORD='§€Cr3t';
const SMTP_SSL= true;
const SMTP_TLS = false;