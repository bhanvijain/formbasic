<?php


namespace BhanviJain\OnlineAgreement;


use Kepawni\Twilted\Basic\ImmutableValue;

/**
 * @property-read string $subject
 * @property-read EmailBody $body
 * @property-read  string $fromAddress
 * @property-read string $toAddress
 * @property-read EmailAttachment $attachment
 *
 * @method self withFromAddress(string $from)
 * @method self withToAddress(string $to)
 * @method self withSubject(string $sub)
 * @method self withBody(EmailBody $body)
 * @method self withAttachment(EmailAttachment $a)
 */
class EmailStructure extends ImmutableValue
{
    /**
     * @param string $fromAddress
     * @param string $toAddress
     * @param string $subject
     * @param EmailBody $body
     * @param EmailAttachment $attachment
     */

    function __construct(string $fromAddress, string $toAddress,
                         string $subject, EmailBody $body,
                         EmailAttachment $attachment)
    {
        $this->init('fromAddress', $fromAddress);
        $this->init('toAddress', $toAddress);
        $this->init('subject', $subject);
        $this->init('body', $body);
        $this->init('attachment', $attachment);

    }

}