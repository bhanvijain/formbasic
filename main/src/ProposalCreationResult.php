<?php

namespace BhanviJain\OnlineAgreement;

use DateTimeInterface;

class ProposalCreationResult
{
    private ?DateTimeInterface $previouslySentOn;
    private ?string $confirmationUrlJustGenerated;
    private ?DateTimeInterface $confirmedOn;
    private ?string $mailingError;
    private bool $isFileUploaded;

    function __construct(DateTimeInterface $sentOn = null, string $confirmationUrl = null,
                         DateTimeInterface $confirmedOn = null, string $mailingError = null, bool $isFileUploaded = false)
    {
        $this->previouslySentOn = $sentOn;
        $this->confirmationUrlJustGenerated = $confirmationUrl;
        $this->confirmedOn = $confirmedOn;
        $this->mailingError = $mailingError;
        $this->isFileUploaded = $isFileUploaded;
    }

    function getPreviouslySentOn(): ?DateTimeInterface
    {
        return $this->previouslySentOn;
    }

    function getConfirmedOn(): ?DateTimeInterface
    {
        return $this->confirmedOn;
    }

    function getConfirmationUrlJustGenerated(): ?string
    {
        return $this->confirmationUrlJustGenerated;
    }

    function getMailingError(): ?string
    {
        return $this->mailingError;
    }

    function isFileUploaded(): bool
    {
        return $this->isFileUploaded;
    }


    function withPreviouslySentOn(?DateTimeInterface $sentOn): self
    {
        return new self($sentOn, $this->confirmationUrlJustGenerated, $this->confirmedOn, $this->mailingError, $this->isFileUploaded);
    }

    function withConfirmedOn(?DateTimeInterface $confirmedOn): self
    {
        return new self($this->previouslySentOn, $this->confirmationUrlJustGenerated, $confirmedOn, $this->mailingError, $this->isFileUploaded);
    }

    function withConfirmationUrlJustGenerated(?string $confirmationUrl): self
    {
        return new self($this->previouslySentOn, $confirmationUrl, $this->confirmedOn, $this->mailingError, $this->isFileUploaded);
    }

    function withMailingError(?string $mailingError): self
    {
        return new self($this->previouslySentOn, $this->confirmationUrlJustGenerated, $this->confirmedOn, $mailingError, $this->isFileUploaded);
    }

    function withIsFileUploaded(bool $isFileUploaded): self
    {
        return new self($this->previouslySentOn, $this->confirmationUrlJustGenerated, $this->confirmedOn, $this->mailingError, $isFileUploaded);
    }
}